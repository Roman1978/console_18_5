﻿#include <clocale>

#include <stack>

#include <iostream>


using namespace std;

struct DATA {
    const char* month; // Месяц постройки дома
    int year; // Год
    float price; //цена
};

int main() {
    setlocale(LC_ALL, "rus");

    stack < DATA > s;

    DATA d = {
      "Июнь",
      2019,
      120.2
    };
    s.push(d);

    d.month, "Июль";
    d.year = 2020;
    d.price = 300.23;
    s.push(d);

    d.month, "Август";
    d.year = 2018;
    d.price = 12000.45;
    s.push(d);

    d.month, "Сентябрь";
    d.year = 2017;
    d.price = 120.78;
    s.push(d);

    d.month, "Январь";
    d.year = 2016;
    d.price = 12300;
    s.push(d);

    cout << "Размер стека: " << s.size() << endl << endl;

    while (!s.empty()) {
        d = s.top();
        cout << "Удаление объекта: " << d.month << " " << d.year << " " << d.price << endl;
        s.pop();
    }

    return 0;

}
